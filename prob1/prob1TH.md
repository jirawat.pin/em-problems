#### [หน้าหลัก](../README.md)

#### [Problem Statement:](prob1.md)
### โจทย์ปัญหา:

กำหนดประจุขนาด $`Q`$ คูลอมบ์ อยู่ที่จุด $`(x_o​,y_o​,z_o)`$ บนเเกน $`xy`$ จงหาความหนาเเน่นของประจุบนเเกนนี้ที่จุด $`(0,0,0)`$

### [วิธีทำ TH](prob1solutionTH.md)
### [Solutions EN](prob1solutionEN.md)
<img src="asset/prob1.png" width="50%" height="50%">
